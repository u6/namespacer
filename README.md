# Vault Police

The script in this repository makes Vault Namespaces,
and sets up provisioning for them "as a service."

Use it when managing Namespaces as data, and 
trigger on approved self service requests. 

But first, let me explain a bit what the difference is between
Namespaces as Code and Namespaces as Data. 

## Vault Namespaces "as Data" vs "as Code":

### Vault Namespaces "as Code"
Vault namespaces can be created and managed from a codebase of Terraform or .json code. 

If you're using Namespaces as code, then you'd use 
this Terraform resource (or .json payload equivalent)  
in an SCM repo: 
https://www.terraform.io/docs/providers/vault/r/namespace.html

Designed to work alongside a system 
like the [Vault API Provisioner](https://gitlab.com/u6/vault-api-provisioner)  
or the Vault Provider for Terraform, because these would  
handle, as code, the non-namespace configuration of Vault via it's API.

### Vault Namespaces "as Data"
However, the namespaces can also be created directly by a GRC tool like ServiceNow, ZenGRC, or BMC. 

The idea of Namespaces as data is that this script would be triggered  
  on a self-service request from something like Rundeck, ServiceNow, BMC,  
Cherwell Software, or other governance and self-service tool.  
That's as opposed to Namespaces as Code, where a Source Code  
change would result in the creation of a new Vault Namespace.

## 2 Approaches for Vault Tenant (Namespace) Governance

Setting up Vault Enterprise is one thing, [supporting its adoption 
as a large scale shared service](https://youtu.be/U9Qr7JtYirA?t=1097)
is another. 

When your application developers want to add a new secrets engine integration,
or change the parameters of an existing one, do they need to make a new ticket?

What if they need to even write a new Vault Policy, or add an authentication
method, because their application needs to authenticate from a new platform?

Namespaces can be used to separate secrets management domains by levels of  
confidentiality, e.g. a prod and non-prod namespace. We can use them to solve
the above maintianability scenarios, too. 

We can also use Namespaces to allow internal groups of developers to 
consume "Tenants" of Vault Enterprise, and expose Vault Enterprise as a
platform on which developers can build.

In terms of reducing the potential operational burden, 
there are two approaches to take: 

### Approach 1 - Vault Namespaces with Secrets Engines Only

In this format, central CI/CD automation tools use data from 
self-service platforms like Rundeck, ServiceNow, BMC, Cherwell  
Software, or other governance and self-service tool.
The automation tools would use the self-service platform  
to identify each separately managed “Product Team” or 
“Self-Managed Team” and grant a namespace with which they can manage their secrets. 

These are pre-created, and simply returned to the consumer after making a self-service 
request with the appropriate management approval through that self-service tool. 

The number of Namespaces in these scenarios may scale to thousands, because Vault Enterprise 
is offered as a cloud platform on which the internal  
consumers can configure "Tenants" to address their own 
Secrets Management needs, rather than a service supported only by a central DevOps group. 

To address the concerns of configuration drift and lack of standardization among the usage 
of the various workspaces, Vault Enterprise administrators set up the following restrictions: 

* The Automatic NameSpace Provisioner grants Internal Vault Consumers authentication 
only at the level of a parent namespace, as demonstrated to customers on October 22.
* Internal Vault Consumers only have access to create and update  
1 or more Secrets Engines, each with a specific allowed name.
* Vault Policy Path Templating is used to reduce the number of Vault Policies that need to be managed.
* Internal Consumers cannot manage Vault Policies within a namespace, 
or, for that matter, any Vault Policies at all, unless they go 
through another self-service and approval process.

### Approach 2 - Fully Delegated Vault Namespaces with Sentinel Guardrails

This way, applications will still use Vault Enterprise similarly to Approach 1.  
And CI/CD tools will still identify the boundaries of each namespace in the same way as Approach 1.    
But some human users are allowed much more control of their own namespace, because this
means they won't have to make tickets for someone else to make changes to their Vault Namespace's configuration 
every time they need to add or change an application's usage of Vault.

The restrictions are enforced, rather, via Sentinel EGPs,  
instead of simply disabling all access except for management of Secrets Engines. 

Both of these approaches enable some minimal "guardrails"  
around self-service for internal adopters of Vault Enterprise. 

Transitioning from approach 1 to approach 2 is simply a matter of  
applying the Sentinel EGPs to all of the Vault Namespaces, then piloting  
the removal of the other non-Sentinel restrictions to a select group of  
internal consumers. After the pilot successfully finishes, the owner of  
the Vault Namespace can have their privileges within that Vault Namespace enhanced.
