#!/usr/bin/env bash

## This is an example of what, in the the README.md file, I call "Approach 1".
## Make sure Vault Enterprise is installed and on your path.


NAMESLUG=$1
LDAPGROUP=$2

# MAKE NAMESPACE
vault namespace create "$NAMESLUG"

# MAKE POLICY IN ROOT NAMESPACE
cat << HEREDOC > "ns-$NAMESLUG.hcl"
path "$NAMESLUG" {
  capabilities = ["sudo", "list", "update", "create", "delete", "read"]
}
HEREDOC

vault policy write "$NAMESLUG" "$NAMESLUG.hcl"


# AUTH FOR POLICY
## This is an authentication role in an LDAP Auth Method in Vault's Root Namespace for using policy a policy that only gives access to one specific namespace.
vault write "auth/ldap/groups/$LDAPGROUP" policies="$NAMESLUG"

# DEFAULT SECRETS ENGINES PER-NAMESPACE
vault secrets enable -namespace="$NAMESLUG" pki
vault secrets enable -namespace="$NAMESLUG" kv
vault secrets enable -namespace="$NAMESLUG" transit

# MACHINE AUTH METHODS
vault auth enable -namespace="$NAMESLUG" cert # This is an example

# DEFAULT EXAMPLE POLICIES

## PKI Secrets Consumer Policy (`secretsconsumer`)
cat << HEREDOC > "$NAMESLUG-secretsconsumer.hcl"
path "$NAMESLUG/pki/roles/frobnicator*" {
  capabilities = ["list", "read"]
}
path "$NAMESLUG/pki/roles/frobnicatorDev*" {
  capabilities = ["list", "read"]
}
HEREDOC
vault policy write -namespace="$NAMESLUG" secretsconsumer "$NAMESLUG-secretsconsumer.hcl"

## PKI Secrets Owner Policy (ownerofsecrets)
cat << HEREDOC > "$NAMESLUG-ownerofsecrets.hcl"
path "$NAMESLUG/pki/roles/frobnicator*" {
  capabilities = ["list", "create", "update"]
}
path "$NAMESLUG/pki/roles/frobnicatorDev*" {
  capabilities = ["list", "create", "update"]
}
HEREDOC
vault policy write -namespace="$NAMESLUG" ownerofsecrets "$NAMESLUG-ownerofsecrets.hcl"

## Owner of PKI Secrets Consumer Policy (`ownerofconsumer` for these secrets, for "devs")
cat << HEREDOC > "$NAMESLUG-ownerofconsumer.hcl"
path "$NAMESLUG/pki/roles/frobnicatorDev*" { ## Note that it's named as a dev role.
  capabilities = ["list", "update", "read", "create", "delete"]
}
HEREDOC
vault policy write -namespace="$NAMESLUG" ownerofconsumer "$NAMESLUG-ownerofconsumer.hcl"

## Secrets Engine Admin (`sadmin`)
cat << HEREDOC > "$NAMESLUG-sadmin.hcl"
path "$NAMESLUG/pki*" {
  capabilities = ["list", "update", "read", "create", "delete"]
}
path "$NAMESLUG/kv*" {
  capabilities = ["list", "update", "read", "create", "delete"]
}
path "$NAMESLUG/transit*" {
  capabilities = ["list", "update", "read", "create", "delete"]
}
path "$NAMESLUG/*" {
  capabilities = ["list", "read", "delete"]
}
## Lack of priviliges for creating or updating Vault Policies and Roles prevents escalation to get full control of namespace.
## Hence the name "sadmin" instead of "admin". He's "sad," because he can't get more privileges, I guess?
HEREDOC
vault policy write -namespace="$NAMESLUG" sadmin "$NAMESLUG-sadmin.hcl"
